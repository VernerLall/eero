from os import listdir
from os.path import isfile, join
from PIL import Image
import numpy as np

def s(x):
    print(x)

path = "./files/"
files = [join(path, f) for f in listdir(path) if isfile(join(path, f))]
bg_imgs = list( filter(lambda x: -1 != x.find('/bg_'), files) )
ov_imgs = list( filter(lambda x: -1 != x.find('/ov_'), files) )
bgs = len(bg_imgs)
ovs = len(ov_imgs)

size = (4081, 4081)
s("Create new images sized %sx%s"%size)
bgMix = Image.new('P', size, 0);
ovMix = Image.new('P', size, 0);

palette32 = [214, 159, 143, 253, 58, 30, 161, 44, 49, 249, 47, 121, 250, 159, 218, 229, 28, 246, 152, 47, 124, 71, 1, 31, 5, 17, 84, 79, 2, 235, 45, 105, 202, 0, 165, 237, 110, 234, 254, 8, 162, 154, 42, 102, 105, 6, 54, 25, 0, 0, 0, 73, 72, 87, 141, 123, 164, 182, 192, 254, 255, 255, 255, 171, 189, 156, 129, 124, 112, 89, 59, 28, 173, 100, 7, 246, 169, 48, 243, 233, 91, 154, 148, 1, 86, 97, 4, 17, 149, 58, 80, 224, 19, 8, 252, 203]
bgMix.putpalette(palette32)
ovMix.putpalette(palette32)

s("Replace all non black pixels with white pixels")

def process(paths, to):
    x = len(paths)
    for path in paths:
        index = int(path.split(".")[1].split("_")[1])
        s("  %s/%s"%(index, x))
        mask = Image.open(path).convert('RGBA')
        data = np.array(mask)
        red, green, blue, alpha = data.T
        black_areas = (red == 0) & (green == 0) & (blue == 0) & (True)
        white_areas = (red != 0) & (green != 0) & (blue != 0) & (True)
        data[...][black_areas.T] = (0, 0, 0, 0)
        data[...][white_areas.T] = (255, 255, 255, 255)  
        mask = Image.fromarray(data)
        to.paste(index, None, mask)

s("Backgrounds")
process(bg_imgs, bgMix)
s("Save bg.png")
bgMix.save('./output/bg.png')
bgMix.close()
s("Foregrounds")    
process(ov_imgs, ovMix)
s("Save ov.png")
ovMix.save('./output/ov.png')
ovMix.close()
